import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./pages/HomePage/HomePage";
import DemoToolkitPage from "./pages/DemoToolkitPage/DemoToolkitPage";
import LoginPage from "./pages/LoginPage/LoginPage";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import MovieMangementPage from "./pages/MovieMangementPage/MovieMangementPage";
import SpinnerComponent from "./components/Spinner";
import { LayoutTheme } from "./HOC/Layout";
function App() {
  return (
    <div className="App">
      <SpinnerComponent />
      <BrowserRouter>
        {/* Routes là nơi quản lý cá route */}
        <Routes>
          <Route path="/" element={<LayoutTheme Component={<HomePage />} />} />
          <Route
            path="/movie"
            element={<LayoutTheme Component={<MovieMangementPage />} />}
          />
          <Route path="/toolkit" element={<DemoToolkitPage />} />
          <Route path="/login" element={<LoginPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
