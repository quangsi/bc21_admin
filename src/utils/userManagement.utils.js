import { Button, message, Tag } from "antd";
import { userService } from "../service/userService";

export const headerTableUsers = [
  {
    title: "Họ tên",
    dataIndex: "hoTen",
    key: "hoTen",
    // render: (text) => <a>{text}</a>,
  },
  {
    title: "Tài khoản",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
    render: (text) => <span className="text-blue-600">{text}</span>,
  },
  {
    title: "Gmail",
    dataIndex: "email",
    key: "email",
    // render: (text) => <a>{text}</a>,
  },
  {
    title: "Loại tài khoản",
    dataIndex: "maLoaiNguoiDung",
    key: "maLoaiNguoiDung",
    render: (text) => {
      if (text == "KhachHang") {
        return <Tag color={"#87d068"}>Khách hàng</Tag>;
      } else {
        return <Tag color={"#f50"}> Quản trị viên</Tag>;
      }
    },
  },
  {
    title: "Tạo tác",
    dataIndex: "action",
    key: "action",
    render: (actionObject, record) => {
      return (
        <>
          {" "}
          <Button
            onClick={() => {
              actionObject.onEdit();
            }}
            type="primary"
          >
            Sửa
          </Button>{" "}
          <Button
            onClick={() => {
              actionObject.onDelete(record.taiKhoan);
              //   userService
              //     .deleteUser(record.taiKhoan)
              //     .then((res) => {
              //       message.success("Xoá thành công");
              //       console.log(res);
              //     })
              //     .catch((err) => {
              //       onSuccess();

              //       console.log(err);
              //       message.error(err.response.data.content);
              //     });
            }}
            type="primary"
            danger
          >
            Xoá
          </Button>{" "}
        </>
      );
    },
  },
];

/**
    {"taiKhoan": "abc123",
    "hoTen": "Hoang Minh",
    "email": "khongco@gmail.com",
    "soDT": "090909090933",
    "matKhau": "123456",
    "maLoaiNguoiDung": "KhachHang",
    action:{getUserList: () => {
    return httpService.get("/api/QuanLyPhim/LayDanhSachPhim");
  },
    }
}
}
 */
