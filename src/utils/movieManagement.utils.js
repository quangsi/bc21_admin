import { Button, message, Tag } from "antd";
import { userService } from "../service/userService";

export const headerTableUsers = [
  {
    title: "Tên phim",
    dataIndex: "tenPhim",
    key: "tenPhim",
    // render: (text) => <a>{text}</a>,
  },
  {
    title: "Bí danh",
    dataIndex: "biDanh",
    key: "biDanh",
    render: (text) => <span className="text-blue-600">{text}</span>,
  },
  {
    title: "Ngày khởi chiếu",
    dataIndex: "ngayKhoiChieu",
    key: "ngayKhoiChieu",
    // render: (text) => <a>{text}</a>,
  },

  // {
  //   title: "Tạo tác",
  //   dataIndex: "action",
  //   key: "action",
  //   render: (actionObject, record) => {
  //     return (
  //       <>
  //         {" "}
  //         <Button
  //           onClick={() => {
  //             actionObject.onEdit();
  //           }}
  //           type="primary"
  //         >
  //           Sửa
  //         </Button>{" "}
  //         <Button
  //           onClick={() => {
  //             actionObject.onDelete(record.taiKhoan);
  //             //   userService
  //             //     .deleteUser(record.taiKhoan)
  //             //     .then((res) => {
  //             //       message.success("Xoá thành công");
  //             //       console.log(res);
  //             //     })
  //             //     .catch((err) => {
  //             //       onSuccess();

  //             //       console.log(err);
  //             //       message.error(err.response.data.content);
  //             //     });
  //           }}
  //           type="primary"
  //           danger
  //         >
  //           Xoá
  //         </Button>{" "}
  //       </>
  //     );
  //   },
  // },
];
