import { Button, Checkbox, Form, Input, message } from "antd";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  setUserLogin,
  setUserLoginService,
} from "../../redux-toolkit/userSlice";
import { userService } from "../../service/userService";

const LoginPage = () => {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    console.log("Success:", values);

    dispatch(setUserLoginService(values))
      .unwrap()
      .then((res) => {
        console.log("res: ", res);
        localStorage.setItem("user", JSON.stringify(res));
        navigate("/");
        // console.log("res unwrap ", res);
      });
    // userService
    //   .dangNhap(values)
    //   .then((res) => {
    //     console.log(res);
    //     dispatch(setUserLogin(res.data.content));

    //     message.success("Đăng nhập thành công");
    //     setTimeout(() => {
    //       // load lại toàn bộ web
    //       //   window.location.href = "/";
    //       // navigate ~ history
    //       navigate("/");
    //     }, 2000);
    //   })
    //   .catch((err) => {
    //     console.log(err);

    //     message.error(err.response.data.content);
    //   });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Form
      name="basic"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 16,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Username"
        name="taiKhoan"
        rules={[
          {
            required: true,
            message: "Please input your username!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="matKhau"
        rules={[
          {
            required: true,
            message: "Please input your password!",
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};

export default LoginPage;
