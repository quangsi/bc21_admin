import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { giamSoLuong, tangSoLuong } from "../../redux-toolkit/demoSlice";

export default function DemoToolkitPage() {
  let { number } = useSelector((state) => {
    return state.demoSlice;
  });
  let dispatch = useDispatch();
  console.log("number: ", number);
  return (
    <div className="container py-20 mx-auto ">
      <button
        onClick={() => {
          dispatch(tangSoLuong(5));
        }}
        className="px-5 py-2 rounded bg-blue-600 text-white"
      >
        Tăng
      </button>
      <span className="mx-10">{number}</span>
      <button
        onClick={() => {
          dispatch(giamSoLuong());
        }}
        className="px-5 py-2 rounded bg-red-600 text-white"
      >
        Giảm
      </button>
    </div>
  );
}
