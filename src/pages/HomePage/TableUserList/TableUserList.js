import { Space, Table, Tag } from "antd";
import { headerTableUsers } from "../../../utils/userManagement.utils";

const TableUserList = ({ userList }) => (
  <Table columns={headerTableUsers} dataSource={userList} />
);

export default TableUserList;
