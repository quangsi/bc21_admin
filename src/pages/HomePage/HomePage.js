import { message } from "antd";
import React, { useEffect, useState } from "react";
import { httpService } from "../../service/configURL";
import { movieService } from "../../service/movieService";
import { userService } from "../../service/userService";
import TableUserList from "./TableUserList/TableUserList";

export default function HomePage() {
  const [dataRaw, setDataRaw] = useState([]);
  let onDeleleSucces = () => {
    console.log("yes");
  };

  let handleDelete = (taiKhoan) => {
    userService
      .deleteUser(taiKhoan)
      .then((res) => {
        message.success("Xoá thành công");
        console.log(res);
      })
      .catch((err) => {
        message.error(err.response.data.content);
        console.log(err);
      });
  };
  useEffect(() => {
    userService
      .getUserList()
      .then((res) => {
        let list = res.data.content.map((item) => {
          return {
            ...item,
            action: {
              onEdit: () => {},
              onDelete: () => {
                handleDelete(item.taiKhoan);
              },
            },
          };
        });
        setDataRaw(list);
        console.log("list: ", list);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <div className="container mx-auto my-10">
        <TableUserList userList={dataRaw} />
      </div>
    </div>
  );
}
