import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import SpinnerComponent from "../../components/Spinner";
import {
  setSpinnerEnded,
  setSpinnerStarted,
} from "../../redux-toolkit/spinnerSlice";
import { movieService } from "../../service/movieService";
import TableMovieList from "./TableMovieList/TableMovieList";

export default function MovieMangementPage() {
  let { loading } = useSelector((state) => state.spinnerSlice);
  let dispatch = useDispatch();

  useEffect(() => {
    // dispatch(setSpinnerStarted());
    movieService
      .getMovieList()
      .then((res) => {
        // dispatch(setSpinnerEnded());
        console.log(res);
      })
      .catch((err) => {
        // dispatch(setSpinnerEnded());
        console.log(err);
      });
  }, []);

  return (
    <div>
      <TableMovieList />
    </div>
  );
}
