import React, { useEffect } from "react";

export default function SecureView({ children }) {
  useEffect(() => {
    let userLocal = JSON.parse(localStorage.getItem("user"));
    if (!userLocal?.accessToken) {
      window.location.href = "/login";
    }
  }, []);
  return <> {children}</>;
}
