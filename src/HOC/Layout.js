import React from "react";
import HeaderTheme from "../components/HeaderTheme/HeaderTheme";
import SiderTheme from "../components/SiderTheme/SiderTheme";
import SecureView from "./SecureView";

export function LayoutTheme({ Component }) {
  return (
    <SecureView>
      <div>
        <div className=" z-20 bg-white fixed w-screen h-20 top-0 left-0">
          <HeaderTheme />
        </div>
        <div id="content_layout" className="flex mt-20">
          <SiderTheme />
          <div className=" p-3 flex justify-center">{Component}</div>
        </div>
      </div>
    </SecureView>
  );
}
