import axios from "axios";
import {
  setSpinnerEnded,
  setSpinnerStarted,
} from "../redux-toolkit/spinnerSlice";
import { storeToolkit } from "../redux-toolkit/store";

export const BASE_URL = "https://movienew.cybersoft.edu.vn";
export const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAyMSIsIkhldEhhblN0cmluZyI6IjEzLzEyLzIwMjIiLCJIZXRIYW5UaW1lIjoiMTY3MDg4OTYwMDAwMCIsIm5iZiI6MTY0MTkyMDQwMCwiZXhwIjoxNjcxMDM3MjAwfQ.bmkH3ZTAY_imW1WGWQrt5UXILbKPSLre4odX6sUKnbU";

let timeRequestMax;

let getAccesstoken = () => {
  let jsonData = localStorage.getItem("user");
  if (jsonData) {
    return JSON.parse(jsonData).accessToken;
  } else {
    return null;
  }
};
export const httpService = axios.create({
  baseURL: BASE_URL,
  timeout: 1000 * timeRequestMax,
  headers: {
    TokenCybersoft: TOKEN_CYBERSOFT,
    Authorization: "Bearer " + getAccesstoken(),
  },
});
// can thiệp trước khi gọi request
httpService.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    storeToolkit.dispatch(setSpinnerStarted());
    console.log("yes request");
    return config;
  },
  function (error) {
    console.log("error: ", error);
    // Do something with request error
    return Promise.reject(error);
  }
);
// can thiệp sau khi có request trả về

httpService.interceptors.response.use(
  function (response) {
    storeToolkit.dispatch(setSpinnerEnded());
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    console.log("error interceptor: ", error);
    // đá ra ngoài login nếu status = 401
    storeToolkit.dispatch(setSpinnerEnded());

    switch (error.response.status) {
      case 401:
      case 403:
        window.location.href = "/login";
    }
    return Promise.reject(error);
  }
);
