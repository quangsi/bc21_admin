import { httpService } from "./configURL";

export const movieService = {
  getMovieList: () => {
    return httpService.get("/api/QuanLyPhim/LayDanhSachPhim");
  },
};
