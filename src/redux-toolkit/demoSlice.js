import { createSlice } from "@reduxjs/toolkit";

let initialState = {
  number: 10,
};

export const demoSlice = createSlice({
  name: "demoSlice",
  initialState,
  reducers: {
    tangSoLuong: (state, { payload }) => {
      state.number += payload;
    },
    giamSoLuong: (state, { payload }) => {
      state.number -= 1;
    },
  },
});

export const { tangSoLuong, giamSoLuong } = demoSlice.actions;

export default demoSlice.reducer;

// gioi thieu ban than
