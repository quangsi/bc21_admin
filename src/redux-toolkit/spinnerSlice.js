import { createSlice } from "@reduxjs/toolkit";

let initialState = {
  loading: false,
  count: 0,
};

let spinnerSlice = createSlice({
  name: "spinnerSlice",
  initialState,
  reducers: {
    setSpinnerStarted: (state, action) => {
      state.loading = true;
      state.count = state.count + 1;
    },
    setSpinnerEnded: (state, action) => {
      state.count = state.count - 1;
      if (state.count == 0) {
        state.loading = false;
      }
    },
  },
});

export let { setSpinnerStarted, setSpinnerEnded } = spinnerSlice.actions;

export default spinnerSlice.reducer;
