import { configureStore } from "@reduxjs/toolkit";
import demoSlice from "./demoSlice";
import spinnerSlice from "./spinnerSlice";
import userSlice from "./userSlice";

export const storeToolkit = configureStore({
  reducer: {
    demoSlice,
    userSlice,
    spinnerSlice,
  },
});
