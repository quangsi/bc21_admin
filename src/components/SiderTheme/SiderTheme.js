import React from "react";
import { NavLink } from "react-router-dom";

export default function SiderTheme() {
  return (
    <div className="w-60 h-screen">
      <div className=" fixed w-60 h-screen top-0 left-0 pt-20 shadow">
        {/* sider item */}
        <p>
          <NavLink className={""} to="/">
            User
          </NavLink>
        </p>
        <p>
          <NavLink className={""} to="/movie">
            Movie
          </NavLink>
        </p>
      </div>
    </div>
  );
}
