import React from "react";

export default function HeaderTheme() {
  return (
    <div className=" shadow h-20 w-full">
      <div className="container mx-auto flex items-center justify-between h-full text-2xl ">
        <span className="text-red-500">AdminMovie</span>

        <button>Username </button>
      </div>
    </div>
  );
}
