import React from "react";
import { useSelector } from "react-redux";
import { PacmanLoader } from "react-spinners";

export default function SpinnerComponent() {
  let { loading } = useSelector((state) => state.spinnerSlice);
  return loading ? (
    <div className="fixed w-screen h-screen top-0 left-0 bg-black z-50 flex justify-center items-center">
      <PacmanLoader color="#FF9F29" loading={true} size={100} />
    </div>
  ) : (
    <></>
  );
}
